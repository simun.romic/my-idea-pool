FROM gradle:5.3.1-jdk8-alpine as builder

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build

FROM openjdk:8u212-jre-slim-stretch

ENV PORT 8081
EXPOSE ${PORT}

COPY --from=builder /home/gradle/src/build/libs/my-idea-pool-0.1.jar /app/

WORKDIR /app

CMD ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-jar", "/app/my-idea-pool-0.1.jar"]