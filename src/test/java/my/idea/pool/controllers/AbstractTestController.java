package my.idea.pool.controllers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.App;
import my.idea.pool.protocols.UserRegistrationRequest;
import my.idea.pool.security.authentication.EmailPasswordCredentials;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;

import javax.validation.constraints.NotNull;
import java.net.MalformedURLException;
import java.net.URL;

@Slf4j
public abstract class AbstractTestController extends App {

    protected static HttpClient getHttpClient() throws MalformedURLException {
       return HttpClient.create(new URL("http://" + server.getHost() + ":" + server.getPort()));
    }

    protected static JwtAccessRefreshToken login(@NotNull String email, @NotNull String password) throws MalformedURLException {
        final JwtAccessRefreshToken jwtAccessRefreshToken = getHttpClient().toBlocking().retrieve(HttpRequest.POST("/access-tokens", new EmailPasswordCredentials(email, password)), JwtAccessRefreshToken.class);
        return jwtAccessRefreshToken;
    }

    public static JwtAccessRefreshToken register(@NotNull UserRegistrationRequest userRegistrationRequest) throws MalformedURLException {
        return getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class);
    }
}
