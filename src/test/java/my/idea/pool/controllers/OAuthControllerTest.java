package my.idea.pool.controllers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import my.idea.pool.protocols.TokenRefreshRequest;
import my.idea.pool.protocols.UserLogoutRequest;
import my.idea.pool.protocols.UserRegistrationRequest;
import my.idea.pool.security.authentication.EmailPasswordCredentials;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;
import my.idea.pool.services.JwtRefreshTokenService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class OAuthControllerTest extends AbstractTestController {

    private static JwtAccessRefreshToken jwtAccessRefreshToken;

    @BeforeAll
    public static void setup() throws MalformedURLException {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test1@test.com");
        userRegistrationRequest.setName("Test");
        userRegistrationRequest.setPassword("Password1@");

        jwtAccessRefreshToken = register(userRegistrationRequest);
    }

    @Test
    void login() throws MalformedURLException {
        getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/access-tokens", new EmailPasswordCredentials("test1@test.com", "Password1@")), JwtAccessRefreshToken.class);

        assertNotNull(jwtAccessRefreshToken);
        assertNotEquals(jwtAccessRefreshToken.getAccessToken().isEmpty(), true);
        assertNotEquals(jwtAccessRefreshToken.getRefreshToken().isEmpty(), true);
    }

    @Test
    void loginInvalidCredentials() {
        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/access-tokens", new EmailPasswordCredentials("test1@test.com", "Password2@")), JwtAccessRefreshToken.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.UNAUTHORIZED);
    }

    @Test
    void refresh() throws MalformedURLException {
        final JwtAccessRefreshToken jwtAccessRefreshToken1 = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/access-tokens/refresh", new TokenRefreshRequest(jwtAccessRefreshToken.getRefreshToken())), JwtAccessRefreshToken.class);

        assertNotNull(jwtAccessRefreshToken1);
        assertEquals(jwtAccessRefreshToken1.getAccessToken().isEmpty(), false);
        assertNull(jwtAccessRefreshToken1.getRefreshToken());
    }

    @Test
    void refreshInvalidRefreshToken() {
        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/access-tokens/refresh", new TokenRefreshRequest(UUID.randomUUID().toString())), JwtAccessRefreshToken.class), "Wrong refresh token!");

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void logout() throws MalformedURLException {
        final JwtRefreshTokenService jwtRefreshTokenService = server.getApplicationContext().findBean(JwtRefreshTokenService.class).get();
        assertNotNull(jwtRefreshTokenService);

        final boolean jwtRefreshTokenExists = jwtRefreshTokenService.refreshTokenExists(jwtAccessRefreshToken.getRefreshToken());
        assertTrue(jwtRefreshTokenExists);

        final HttpResponse<Object> response = getHttpClient()
                .toBlocking()
                .exchange(HttpRequest.DELETE("/access-tokens", new UserLogoutRequest(jwtAccessRefreshToken.getRefreshToken())).header("x-access-token", jwtAccessRefreshToken.getAccessToken()));
        assertEquals(response.getStatus(), HttpStatus.NO_CONTENT);

        final boolean jwtRefreshTokenNotExists = jwtRefreshTokenService.refreshTokenExists(jwtAccessRefreshToken.getRefreshToken());
        assertEquals(jwtRefreshTokenNotExists, false);
    }

    @Test
    void logoutUnauthorized() {
        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().exchange(HttpRequest.DELETE("/access-tokens")));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.UNAUTHORIZED);
    }
}