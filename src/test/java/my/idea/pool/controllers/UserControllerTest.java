package my.idea.pool.controllers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import my.idea.pool.protocols.UserDto;
import my.idea.pool.protocols.UserRegistrationRequest;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest
class UserControllerTest extends AbstractTestController {

    @Test
    void registerMissingEmailBadRequest() {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setName("Test");
        userRegistrationRequest.setPassword("Password1@");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class), "email must not be blank");

        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void registerMissingNameBadRequest() {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test@test.com");
        userRegistrationRequest.setPassword("Password1@");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class), "name must not be blank");

        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void registerInvalidEmailBadRequest() {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test");
        userRegistrationRequest.setName("Test");
        userRegistrationRequest.setPassword("Password1@");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class), "invalid email format");

        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void registerMissingPasswordBadRequest() {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test");
        userRegistrationRequest.setName("Test");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class), "password must not be blank");

        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void registerInvalidPasswordBadRequest() {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test");
        userRegistrationRequest.setName("Test");
        userRegistrationRequest.setPassword("pass");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class), "Invalid Password");

        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void registerValid() throws MalformedURLException {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test@test.com");
        userRegistrationRequest.setName("Test");
        userRegistrationRequest.setPassword("Password1@");

        final JwtAccessRefreshToken jwtAccessRefreshToken = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/users", userRegistrationRequest), JwtAccessRefreshToken.class);

        assertNotNull(jwtAccessRefreshToken.getAccessToken());
        assertNotNull(jwtAccessRefreshToken.getRefreshToken());
    }

    @Test
    void me() throws MalformedURLException {

        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test29@test.com");
        userRegistrationRequest.setName("Test29");
        userRegistrationRequest.setPassword("Password1@");

        final JwtAccessRefreshToken jwtAccessRefreshToken = register(userRegistrationRequest);

        final UserDto userDto = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.GET("/me").header("x-access-token", jwtAccessRefreshToken.getAccessToken()), UserDto.class);
        assertNotNull(userDto);
        assertEquals("test29@test.com", userDto.getEmail());
        assertEquals("Test29", userDto.getName());
    }

    @Test
    void meUnauthorized() {
        final HttpClientResponseException unauthorized = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.GET("/me"), UserDto.class),
                "Unauthorized");
        assertEquals(unauthorized.getStatus(), HttpStatus.UNAUTHORIZED);
    }
}