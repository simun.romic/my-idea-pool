package my.idea.pool.controllers;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.protocols.IdeaCreateRequest;
import my.idea.pool.protocols.IdeaDto;
import my.idea.pool.protocols.IdeaUpdateRequest;
import my.idea.pool.protocols.UserRegistrationRequest;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@Requires(property="ideas.max-page-size")
@MicronautTest
class IdeaControllerTest extends AbstractTestController {
    @Value("${ideas.max-page-size}")
    private int maxPageSize;

    private static JwtAccessRefreshToken jwtAccessRefreshToken;

    @BeforeAll
    public static void setup() throws MalformedURLException {
        final UserRegistrationRequest userRegistrationRequest = new UserRegistrationRequest();
        userRegistrationRequest.setEmail("test23@test.com");
        userRegistrationRequest.setName("Test");
        userRegistrationRequest.setPassword("Password1@");

        jwtAccessRefreshToken = register(userRegistrationRequest);
    }

    @Test
    void create() throws MalformedURLException {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setConfidence(1);
        ideaCreateRequest.setContent("Something");
        ideaCreateRequest.setEase(1);
        ideaCreateRequest.setImpact(1);

        final IdeaDto ideaDto = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class);

        assertNotNull(ideaDto);
        assertEquals(ideaCreateRequest.getContent(), ideaDto.getContent());
        assertEquals(ideaCreateRequest.getConfidence(), ideaDto.getConfidence());
        assertEquals(ideaCreateRequest.getEase(), ideaDto.getEase());
        assertEquals(ideaCreateRequest.getImpact(), ideaDto.getImpact());
        assertNotNull(ideaDto.getAverageScore());
        assertEquals(new Double(1.0), ideaDto.getAverageScore());
        assertNotNull(ideaDto.getCreatedAt());
    }

    @Test
    void createMissingContent() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setConfidence(1);
        ideaCreateRequest.setEase(1);
        ideaCreateRequest.setImpact(1);

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void createMissingEase() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setConfidence(1);
        ideaCreateRequest.setImpact(1);
        ideaCreateRequest.setContent("Something");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void createMissingImpact() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setConfidence(1);
        ideaCreateRequest.setEase(1);
        ideaCreateRequest.setContent("Something");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void createMissingConfidence() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setEase(1);
        ideaCreateRequest.setImpact(1);
        ideaCreateRequest.setContent("Something");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void createInvalidConfidenceValue() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setEase(1);
        ideaCreateRequest.setImpact(1);
        ideaCreateRequest.setConfidence(12);
        ideaCreateRequest.setContent("Something");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void createInvalidEaseValue() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setEase(12);
        ideaCreateRequest.setImpact(1);
        ideaCreateRequest.setConfidence(1);
        ideaCreateRequest.setContent("Something");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }


    @Test
    void createInvalidImpactValue() {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setEase(1);
        ideaCreateRequest.setImpact(11);
        ideaCreateRequest.setConfidence(1);
        ideaCreateRequest.setContent("Something");

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }

    @Test
    void delete() throws MalformedURLException {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setConfidence(2);
        ideaCreateRequest.setContent("Something");
        ideaCreateRequest.setEase(2);
        ideaCreateRequest.setImpact(2);

        final IdeaDto ideaDto = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class);

        assertNotNull(ideaDto);

        final HttpResponse<Object> response = getHttpClient()
                .toBlocking()
                .exchange(HttpRequest.DELETE("/ideas/" + ideaDto.getId()).header("x-access-token", jwtAccessRefreshToken.getAccessToken()));

        assertNotNull(response);
        assertEquals(response.getStatus(), HttpStatus.NO_CONTENT);
    }

    @Test
    void deleteWrongId() {
        final IdeaUpdateRequest ideaUpdateRequest = new IdeaUpdateRequest();
        ideaUpdateRequest.setConfidence(2);
        ideaUpdateRequest.setContent("Something");
        ideaUpdateRequest.setEase(2);
        ideaUpdateRequest.setImpact(2);

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.PUT("/ideas/"+UUID.randomUUID().toString(), ideaUpdateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    void update() throws MalformedURLException {
        final IdeaCreateRequest ideaCreateRequest = new IdeaCreateRequest();
        ideaCreateRequest.setConfidence(2);
        ideaCreateRequest.setContent("Something");
        ideaCreateRequest.setEase(2);
        ideaCreateRequest.setImpact(2);

        final IdeaDto ideaDto = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.POST("/ideas", ideaCreateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class);

        assertNotNull(ideaDto);

        final IdeaUpdateRequest ideaUpdateRequest = new IdeaUpdateRequest();
        ideaUpdateRequest.setConfidence(3);
        ideaUpdateRequest.setContent("Something2");
        ideaUpdateRequest.setEase(3);
        ideaUpdateRequest.setImpact(3);

        final IdeaDto ideaDtoUpdated = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.PUT("/ideas/" + ideaDto.getId(), ideaUpdateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class);

        assertNotNull(ideaDtoUpdated);
        assertEquals(ideaUpdateRequest.getConfidence(), ideaDtoUpdated.getConfidence());
        assertEquals(ideaUpdateRequest.getContent(), ideaDtoUpdated.getContent());
        assertEquals(ideaUpdateRequest.getEase(), ideaDtoUpdated.getEase());
        assertEquals(ideaUpdateRequest.getImpact(), ideaDtoUpdated.getImpact());
        assertEquals(new Double(3.0), ideaDtoUpdated.getAverageScore());
    }

    @Test
    void updateWrongId() {
        final IdeaUpdateRequest ideaUpdateRequest = new IdeaUpdateRequest();
        ideaUpdateRequest.setConfidence(2);
        ideaUpdateRequest.setContent("Something");
        ideaUpdateRequest.setEase(2);
        ideaUpdateRequest.setImpact(2);

        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.PUT("/ideas/"+UUID.randomUUID().toString(), ideaUpdateRequest).header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.NOT_FOUND);
    }

    @Test
    void search() throws MalformedURLException {
        for(int i = 0; i < this.maxPageSize+1; i++) {
            create();
        }

        final List<IdeaDto> ideas = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.GET("/ideas?page=1").header("x-access-token", jwtAccessRefreshToken.getAccessToken()), List.class);

        assertNotNull(ideas);
        assertEquals(ideas.size(), this.maxPageSize);

        final List<IdeaDto> ideas2 = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.GET("/ideas?page=2").header("x-access-token", jwtAccessRefreshToken.getAccessToken()), List.class);

        assertNotNull(ideas2);
        assertEquals(ideas2.size(), 2);

        final List<IdeaDto> ideas3 = getHttpClient()
                .toBlocking()
                .retrieve(HttpRequest.GET("/ideas?page=3").header("x-access-token", jwtAccessRefreshToken.getAccessToken()), List.class);

        assertNotNull(ideas3);
        assertEquals(ideas3.size(), 0);
    }

    @Test
    void searchInvalidPageNumber() {
        final HttpClientResponseException httpClientResponseException = assertThrows(HttpClientResponseException.class,
                () -> getHttpClient().toBlocking().retrieve(HttpRequest.GET("/ideas?page=0").header("x-access-token", jwtAccessRefreshToken.getAccessToken()), IdeaDto.class));

        assertNotNull(httpClientResponseException);
        assertEquals(httpClientResponseException.getStatus(), HttpStatus.BAD_REQUEST);
    }
}