package my.idea.pool;

import io.micronaut.context.ApplicationContext;
import io.micronaut.runtime.server.EmbeddedServer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App {
    protected static EmbeddedServer server = ApplicationContext.run(EmbeddedServer.class);
}
