package my.idea.pool.services;

import io.micronaut.test.annotation.MicronautTest;
import my.idea.pool.Application;
import my.idea.pool.models.User;
import my.idea.pool.repositories.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@MicronautTest(application=Application.class)
class UserServiceTest {

    @Inject
    UserRepository userRepository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findById() {
        final Optional<User> pero = this.userRepository.findById("pero");
        assertEquals(Optional.empty(), pero);
    }

    @Test
    void byId() {
    }

    @Test
    void create() {
    }

    @Test
    void update() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void create1() {
    }

    @Test
    void exists() {
    }
}