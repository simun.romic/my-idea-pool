package my.idea.pool.security.jwt.render;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

/**
 * Stores the combination of JWT access and refresh tokens.
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Introspected
public class JwtAccessRefreshToken {

    @JsonProperty("jwt")
    @NotNull
    private String accessToken;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("refresh_token")
    @Nullable
    private String refreshToken;

}
