package my.idea.pool.security.jwt;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.util.StringUtils;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.security.authentication.AuthenticationException;
import io.micronaut.security.authentication.AuthenticationFailed;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.handlers.LoginHandler;
import io.micronaut.security.token.jwt.bearer.AccessRefreshTokenLoginHandler;
import io.micronaut.security.token.jwt.config.JwtConfigurationProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import my.idea.pool.security.jwt.generator.JwtAccessRefreshTokenGenerator;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;
import my.idea.pool.services.JwtRefreshTokenService;

import javax.inject.Singleton;
import java.util.Optional;

@Replaces(bean = AccessRefreshTokenLoginHandler.class)
@Requires(property = JwtConfigurationProperties.PREFIX + ".enabled", notEquals = StringUtils.FALSE)
@AllArgsConstructor
@Getter
@Singleton
public class JwtAccessRefreshTokenLoginHandler implements LoginHandler {

    protected final JwtAccessRefreshTokenGenerator accessRefreshTokenGenerator;

    protected final JwtRefreshTokenService jwtRefreshTokenService;

    @Override
    public HttpResponse loginSuccess(UserDetails userDetails, HttpRequest<?> request) {
        final Optional<JwtAccessRefreshToken> accessRefreshTokenOptional = this.accessRefreshTokenGenerator.generateJwt(userDetails);
        this.jwtRefreshTokenService.create(userDetails.getUsername(), accessRefreshTokenOptional);

        return accessRefreshTokenOptional
                .map(HttpResponse::created)
                .orElseGet(HttpResponse::serverError);
    }

    @Override
    public HttpResponse loginFailed(AuthenticationFailed authenticationFailed) {
        throw new AuthenticationException(authenticationFailed.getMessage().orElse(null));
    }

}
