package my.idea.pool.security.jwt.render;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.token.jwt.render.BearerTokenRenderer;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;

@Replaces(bean = BearerTokenRenderer.class)
@Singleton
public class JwtTokenRenderImpl implements JwtTokenRenderer {

    @Override
    public JwtAccessRefreshToken render(Integer expiresIn, @NotNull  String jwtToken, @NotNull  String refreshToken) {
        return new JwtAccessRefreshToken(jwtToken, refreshToken);
    }

    @Override
    public JwtAccessRefreshToken render(UserDetails userDetails, Integer expiresIn, @NotNull String jwtToken, @NotNull String refreshToken) {
        return render(expiresIn, jwtToken, refreshToken);
    }
}
