package my.idea.pool.security.jwt.render;

import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.token.jwt.render.AccessRefreshToken;

/**
 * Responsible for converting token information to an {@link JwtAccessRefreshToken}.
 */
public interface JwtTokenRenderer {
    /**
     * @param expiresIn In milliseconds
     * @param jwtToken JWT token
     * @param refreshToken JWT token
     * @return instance of {@link AccessRefreshToken}
     */
    JwtAccessRefreshToken render(Integer expiresIn, String jwtToken, String refreshToken);

    /**
     *
     * @param userDetails Authenticated user's representation.
     * @param expiresIn In milliseconds
     * @param jwtToken  JWT token
     * @param refreshToken JWT token
     * @return instance of {@link AccessRefreshToken}
     */
    JwtAccessRefreshToken render(UserDetails userDetails, Integer expiresIn, String jwtToken, String refreshToken);

}
