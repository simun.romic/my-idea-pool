package my.idea.pool.security.jwt.generator;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.context.event.ApplicationEventPublisher;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.token.generator.TokenGenerator;
import io.micronaut.security.token.jwt.event.AccessTokenGeneratedEvent;
import io.micronaut.security.token.jwt.event.RefreshTokenGeneratedEvent;
import io.micronaut.security.token.jwt.generator.AccessRefreshTokenGenerator;
import io.micronaut.security.token.jwt.generator.JwtGeneratorConfiguration;
import io.micronaut.security.token.jwt.generator.claims.ClaimsGenerator;
import io.micronaut.security.token.jwt.render.TokenRenderer;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;
import my.idea.pool.security.jwt.render.JwtTokenRenderer;

import javax.inject.Singleton;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Generates http responses with JWT access and refresh token.
 *
 */
@Replaces(bean = AccessRefreshTokenGenerator.class)
@Singleton
@Slf4j
public class JwtAccessRefreshTokenGenerator extends AccessRefreshTokenGenerator {

    protected final JwtTokenRenderer jwtTokenRenderer;

    /**
     *
     * @param jwtGeneratorConfiguration Instance of {@link JwtGeneratorConfiguration}
     * @param tokenRenderer Instance of {@link JwtTokenRenderer}
     * @param tokenGenerator Intance of {@link TokenGenerator}
     * @param claimsGenerator Claims generator
     * @param eventPublisher The Application event publiser
     */
    public JwtAccessRefreshTokenGenerator(JwtGeneratorConfiguration jwtGeneratorConfiguration,
                                          JwtTokenRenderer jwtTokenRenderer,
                                          TokenRenderer tokenRenderer,
                                          TokenGenerator tokenGenerator,
                                          ClaimsGenerator claimsGenerator,
                                          ApplicationEventPublisher eventPublisher) {
        super(jwtGeneratorConfiguration, tokenRenderer, tokenGenerator, claimsGenerator, eventPublisher);
        this.jwtTokenRenderer = jwtTokenRenderer;
    }

    /**
     * Generate an {@link JwtAccessRefreshToken} response for the given
     * user details.
     *
     * @param userDetails Authenticated user's representation.
     * @return The http response
     */
    public Optional<JwtAccessRefreshToken> generateJwt(UserDetails userDetails) {
        final Optional<String> accessToken = tokenGenerator.generateToken(userDetails, jwtGeneratorConfiguration.getAccessTokenExpiration());
        final Optional<String> refreshToken = tokenGenerator.generateToken(userDetails, jwtGeneratorConfiguration.getRefreshTokenExpiration());
        if (!accessToken.isPresent() || !refreshToken.isPresent()) {
            if (log.isDebugEnabled()) {
                if (!accessToken.isPresent()) {
                    log.debug("tokenGenerator failed to generate access token for userDetails {}", userDetails.getUsername());
                }
                if (!refreshToken.isPresent()) {
                    log.debug("tokenGenerator failed to generate refreshToken token for userDetails {}", userDetails.getUsername());
                }
            }
            return Optional.empty();
        }

        final JwtAccessRefreshToken jwtAccessRefreshToken = jwtTokenRenderer.render(userDetails, jwtGeneratorConfiguration.getAccessTokenExpiration(), accessToken.get(), refreshToken.get());
        eventPublisher.publishEvent(new AccessTokenGeneratedEvent(jwtAccessRefreshToken.getAccessToken()));
        eventPublisher.publishEvent(new RefreshTokenGeneratedEvent(jwtAccessRefreshToken.getRefreshToken()));
        return Optional.of(jwtAccessRefreshToken);
    }

    /**
     * Generate an {@link JwtAccessRefreshToken} response for the given
     * refresh token and claims.
     *
     * @param oldClaims The claims to generate the access token
     * @param includeRefreshToken should render refresh token
     * @return The http response
     */
    public Optional<JwtAccessRefreshToken> generateJwt(Map<String, Object> oldClaims, boolean includeRefreshToken) {
        final Map<String, Object> claims = claimsGenerator.generateClaimsSet(oldClaims, jwtGeneratorConfiguration.getAccessTokenExpiration());
        log.info("claims: {}", claims);

        final Optional<String> optionalAccessToken = tokenGenerator.generateToken(claims);
        Optional<String> optionalRefreshToken = Optional.empty();

        if (includeRefreshToken)
            optionalRefreshToken = tokenGenerator.generateToken(claims);

        if (!optionalAccessToken.isPresent()) {
            if (log.isDebugEnabled()) {
                log.info("tokenGenerator failed to generate access token claims: {}", claims.entrySet()
                        .stream()
                        .map((entry) -> entry.getKey() + "=>" + entry.getValue().toString())
                        .collect(Collectors.joining(", ")));
            }
            return Optional.empty();
        }
        final String accessToken = optionalAccessToken.get();
        final String refreshToken = optionalRefreshToken.orElse(null);
        eventPublisher.publishEvent(new AccessTokenGeneratedEvent(accessToken));
        return Optional.of(jwtTokenRenderer.render(jwtGeneratorConfiguration.getAccessTokenExpiration(), accessToken, refreshToken));
    }

    /**
     * Generate an {@link JwtAccessRefreshToken} response for the given
     * refresh token and claims.
     * @param oldClaims The claims to generate the access token
     * @return The http response
     */
    public Optional<JwtAccessRefreshToken> generateJwt(Map<String, Object> oldClaims) {
        final Map<String, Object> claims = claimsGenerator.generateClaimsSet(oldClaims, jwtGeneratorConfiguration.getAccessTokenExpiration());

        final Optional<String> optionalAccessToken = tokenGenerator.generateToken(claims);
        final Optional<String> optionalRefreshToken = tokenGenerator.generateToken(claims);
        if (!optionalAccessToken.isPresent() && !optionalRefreshToken.isPresent()) {
            if (log.isDebugEnabled()) {
                log.info("tokenGenerator failed to generate access token claims: {}", claims.entrySet()
                        .stream()
                        .map((entry) -> entry.getKey() + "=>" + entry.getValue().toString())
                        .collect(Collectors.joining(", ")));
            }
            return Optional.empty();
        }

        final String accessToken = optionalAccessToken.get();
        final String refreshToken = optionalRefreshToken.get();
        final JwtAccessRefreshToken jwtAccessRefreshToken = jwtTokenRenderer.render(jwtGeneratorConfiguration.getAccessTokenExpiration(), accessToken, refreshToken);
        eventPublisher.publishEvent(new AccessTokenGeneratedEvent(jwtAccessRefreshToken.getAccessToken()));
        eventPublisher.publishEvent(new RefreshTokenGeneratedEvent(jwtAccessRefreshToken.getRefreshToken()));
        return Optional.of(jwtAccessRefreshToken);
    }
}
