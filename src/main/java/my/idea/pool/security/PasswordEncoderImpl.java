package my.idea.pool.security;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.security.authentication.providers.PasswordEncoder;
import lombok.AllArgsConstructor;

import javax.inject.Singleton;

@Replaces(bean = PasswordEncoder.class)
@Singleton
@AllArgsConstructor
public class PasswordEncoderImpl implements PasswordEncoder {

    protected final org.springframework.security.crypto.password.PasswordEncoder passwordEncoder;

    @Override
    public String encode(String rawPassword) {
        return this.passwordEncoder.encode(rawPassword);
    }

    @Override
    public boolean matches(String rawPassword, String encodedPassword) {
        return this.passwordEncoder.matches(rawPassword, encodedPassword);
    }
}
