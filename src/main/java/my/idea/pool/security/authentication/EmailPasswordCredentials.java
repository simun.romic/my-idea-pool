package my.idea.pool.security.authentication;

import io.micronaut.security.authentication.AuthenticationRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Represents login request
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmailPasswordCredentials implements Serializable, AuthenticationRequest<String, String> {

    @NotNull
    private String email;

    @NotBlank
    @NotNull
    private String password;

    @Override
    public String getIdentity() { return getEmail(); }

    @Override
    public String getSecret() {
        return getPassword();
    }
}
