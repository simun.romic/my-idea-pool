package my.idea.pool.security;

import io.micronaut.security.authentication.providers.UserFetcher;
import io.micronaut.security.authentication.providers.UserState;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.services.UserService;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;

@Singleton
@AllArgsConstructor
@Slf4j
public class UserFetcherImpl implements UserFetcher {

    private final UserService userService;

    @Override
    public Publisher<UserState> findByUsername(@NotNull String username) {
        log.info("findByUsername: {}", username);

        return Flowable.fromCallable(() -> this.userService.findById(username))
                .subscribeOn(Schedulers.io())
                .flatMap(optionalUser -> optionalUser.map(Flowable::just).orElseGet(Flowable::empty))
                .cast(UserState.class);
    }
}
