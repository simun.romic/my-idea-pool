package my.idea.pool.security;

import io.micronaut.security.authentication.providers.AuthoritiesFetcher;
import io.reactivex.Flowable;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

@Singleton
@Slf4j
public class AuthoritiesFetcherImpl implements AuthoritiesFetcher {
    @Override
    public Publisher<List<String>> findAuthoritiesByUsername(@NotNull String username) {
        log.info("Searching authorities for user: {}", username);
        return Flowable.just(Arrays.asList("ROLE_USER"));
    }
}
