package my.idea.pool.services;

import io.micronaut.security.authentication.providers.PasswordEncoder;
import io.micronaut.spring.tx.annotation.Transactional;
import lombok.AllArgsConstructor;
import my.idea.pool.models.User;
import my.idea.pool.protocols.UserDto;
import my.idea.pool.protocols.UserRegistrationRequest;
import my.idea.pool.repositories.UserRepository;
import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Singleton
@AllArgsConstructor
@Transactional
public class UserService implements GenericService<User, String> {

    protected final UserRepository userRepository;

    protected final PasswordEncoder passwordEncoder;

    protected final GravatarService gravatarService;

    @Transactional(readOnly = true)
    @Override
    public Optional<User> findById(@NotNull String id) {
        return this.userRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public Optional<UserDto> byId(@NotNull String id) {
        return this.userRepository.getDtoById(id);
    }

    @Override
    public User create(@NotNull User user) {
        return this.userRepository.create(user);
    }

    @Override
    public User update(@NotNull String id, @NotNull User user) {
        return this.userRepository.update(user);
    }

    @Override
    public void deleteById(@NotNull String id) {
        this.userRepository.deleteById(id);
    }

    /**
     * Create new user from UserRegistrationRequest
     * @param userRegistrationRequest
     * @return User
     */
    public User create(@NotNull UserRegistrationRequest userRegistrationRequest) {
        final User user = User.builder()
                .email(userRegistrationRequest.getEmail())
                .name(userRegistrationRequest.getName())
                .avatarUrl(gravatarService.create(userRegistrationRequest.getEmail()))
                .password(passwordEncoder.encode(userRegistrationRequest.getPassword()))
                .build();
        return this.userRepository.create(user);
    }

    @Transactional(readOnly = true)
    public boolean exists(@NotNull String id) {
        return this.userRepository.exists(id);
    }
}
