package my.idea.pool.services;

import io.micronaut.spring.tx.annotation.Transactional;
import lombok.AllArgsConstructor;
import my.idea.pool.models.JwtRefreshToken;
import my.idea.pool.models.User;
import my.idea.pool.repositories.JwtRefreshTokenRepository;
import my.idea.pool.repositories.UserRepository;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@AllArgsConstructor
@Singleton
@Transactional
public class JwtRefreshTokenService implements GenericService<JwtRefreshToken, Long> {

    protected JwtRefreshTokenRepository jwtRefreshTokenRepository;

    protected UserRepository userRepository;

    @Transactional(readOnly = true)
    @Override
    public Optional<JwtRefreshToken> findById(@NotNull Long id) {
        return this.jwtRefreshTokenRepository.findById(id);
    }

    @Override
    public JwtRefreshToken create(@NotNull JwtRefreshToken jwtRefreshToken) {
        return this.jwtRefreshTokenRepository.create(jwtRefreshToken);
    }

    @Override
    public JwtRefreshToken update(@NotNull Long id, @NotNull JwtRefreshToken jwtRefreshToken) {
        return this.jwtRefreshTokenRepository.update(jwtRefreshToken);
    }

    @Override
    public void deleteById(@NotNull Long id) {
        this.jwtRefreshTokenRepository.deleteById(id);
    }

    public JwtRefreshToken create(@NotNull String userId, @NotNull Optional<JwtAccessRefreshToken> jwtAccessRefreshToken) {
        final User user = this.userRepository.findById(userId).orElseThrow(RuntimeException::new);
        final JwtAccessRefreshToken jwtTokens = jwtAccessRefreshToken.orElseThrow(RuntimeException::new);

        final JwtRefreshToken jwtRefreshToken = JwtRefreshToken.builder()
                .refreshToken(jwtTokens.getRefreshToken())
                .user(user)
                .build();

        return this.jwtRefreshTokenRepository.create(jwtRefreshToken);
    }

    public void deleteByRefreshToken(@NotNull String refreshToken) {
        this.jwtRefreshTokenRepository.deleteByRefreshToken(refreshToken);
    }

    public boolean refreshTokenExists(@NotNull String refreshToken) {
        return this.jwtRefreshTokenRepository.refreshTokenExists(refreshToken);
    }
}
