package my.idea.pool.services;

import io.micronaut.spring.tx.annotation.Transactional;
import lombok.AllArgsConstructor;
import my.idea.pool.models.Idea;
import my.idea.pool.models.User;
import my.idea.pool.protocols.IdeaCreateRequest;
import my.idea.pool.protocols.IdeaDto;
import my.idea.pool.protocols.IdeaUpdateRequest;
import my.idea.pool.repositories.IdeaRepository;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Singleton
@AllArgsConstructor
@Transactional
public class IdeaService implements GenericService<Idea, UUID> {
    protected final IdeaRepository ideaRepository;

    @Transactional(readOnly = true)
    @Override
    public Optional<Idea> findById(@NotNull UUID id) {
        return this.ideaRepository.findById(id);
    }

    @Override
    public Idea create(@NotNull Idea idea) {
        return this.ideaRepository.create(idea);
    }

    public IdeaDto create(@NotNull IdeaCreateRequest ideaCreateRequest, @NotNull User user) {
        final Idea idea = Idea.builder()
                .confidence(ideaCreateRequest.getConfidence())
                .content(ideaCreateRequest.getContent())
                .ease(ideaCreateRequest.getEase())
                .impact(ideaCreateRequest.getImpact())
                .averageScore((double) (ideaCreateRequest.getConfidence() + ideaCreateRequest.getEase() + ideaCreateRequest.getImpact()) / 3)
                .createdAt(new Date())
                .user(user)
                .build();

        this.ideaRepository.create(idea);

        return IdeaDto.builder()
                .id(idea.getId())
                .averageScore(idea.getAverageScore())
                .confidence(idea.getConfidence())
                .ease(idea.getEase())
                .content(idea.getContent())
                .impact(idea.getImpact())
                .createdAt(idea.getCreatedAt())
                .build();
    }

    @Override
    public Idea update(@NotNull UUID id, @NotNull Idea idea) {
        return this.ideaRepository.update(idea);
    }

    /**
     * Update idea instance from idea update request
     * @param id
     * @param idea
     * @param updateRequest
     * @return updated idea dto instance
     */
    public IdeaDto update(@NotNull UUID id, @NotNull Idea idea, @NotNull IdeaUpdateRequest updateRequest) {
        idea.setConfidence(updateRequest.getConfidence());
        idea.setContent(updateRequest.getContent());
        idea.setEase(updateRequest.getEase());
        idea.setImpact(updateRequest.getImpact());
        idea.setAverageScore((double)(updateRequest.getConfidence() + updateRequest.getEase() + updateRequest.getImpact())/3);

        this.ideaRepository.update(idea);

        return IdeaDto.builder()
                .id(idea.getId())
                .averageScore(idea.getAverageScore())
                .confidence(idea.getConfidence())
                .ease(idea.getEase())
                .content(idea.getContent())
                .impact(idea.getImpact())
                .createdAt(idea.getCreatedAt())
                .build();
    }

    @Override
    public void deleteById(@NotNull UUID id) {
        this.ideaRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<IdeaDto> search(int page) {
        return this.ideaRepository.search(page);
    }
}
