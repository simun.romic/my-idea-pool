package my.idea.pool.services;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.retry.annotation.CircuitBreaker;
import my.idea.pool.utils.HashUtils;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Singleton
@Requires(property="gravatar.url")
public class GravatarService {
    @Value("${gravatar.url}")
    private String GRAVATAR_URL;

    @CircuitBreaker
    public String create(@NotNull String email) {
        return GRAVATAR_URL + "/" + HashUtils.createHash(email).orElse(UUID.randomUUID().toString());
    }
}
