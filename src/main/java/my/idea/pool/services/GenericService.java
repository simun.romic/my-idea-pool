package my.idea.pool.services;

import io.micronaut.spring.tx.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Transactional
public interface GenericService<T, K> {
    Optional<T> findById(@NotNull K id);

    T create(@NotNull T t);

    T update(@NotNull K id, @NotNull T t);

    void deleteById(@NotNull K id);
}
