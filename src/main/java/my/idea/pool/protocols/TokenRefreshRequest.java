package my.idea.pool.protocols;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Represents refresh jwt request
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TokenRefreshRequest implements Serializable {
    @NotBlank
    @JsonProperty("refresh_token")
    private String refreshToken;
}
