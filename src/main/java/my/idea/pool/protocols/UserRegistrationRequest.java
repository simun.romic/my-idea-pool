package my.idea.pool.protocols;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import my.idea.pool.security.constrains.ValidPassword;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Represents user registration request
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper=true)
public final class UserRegistrationRequest extends UserDto implements Serializable {
    @NotBlank
    @ValidPassword
    private String password;
}
