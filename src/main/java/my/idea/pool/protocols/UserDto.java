package my.idea.pool.protocols;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Represents DTO user object, only show some user data
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto implements Serializable {

    @NotBlank
    @Email
    private String email;

    @NotBlank
    private String name;

    @JsonProperty("avatar_url")
    private String avatarUrl;
}
