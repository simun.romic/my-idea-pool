package my.idea.pool.protocols;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper=false)
public class IdeaUpdateRequest extends IdeaCreateRequest {
}
