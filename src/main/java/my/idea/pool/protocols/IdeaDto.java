package my.idea.pool.protocols;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IdeaDto implements Serializable {
    @NotNull
    private UUID id;

    @Size(max = 255)
    private String content;

    @Min(0)
    @Max(10)
    @NotNull
    private Integer impact;

    @Min(0)
    @Max(10)
    @NotNull
    private Integer ease;

    @Min(0)
    @Max(10)
    @NotNull
    private Integer confidence;

    @NotNull
    @JsonProperty("average_score")
    private Double averageScore;

    @JsonProperty("created_at")
    private Date createdAt;
}
