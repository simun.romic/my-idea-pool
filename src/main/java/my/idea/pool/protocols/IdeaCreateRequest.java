package my.idea.pool.protocols;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class IdeaCreateRequest implements Serializable {
    @NotNull
    @Size(max = 255)
    private String content;

    @Min(1)
    @Max(10)
    @NotNull
    private Integer impact;

    @Min(1)
    @Max(10)
    @NotNull
    private Integer ease;

    @Min(1)
    @Max(10)
    @NotNull
    private Integer confidence;
}
