package my.idea.pool.handlers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.core.convert.exceptions.ConversionErrorException;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.hateoas.Link;
import io.micronaut.security.authentication.AuthenticationException;
import io.micronaut.web.router.exceptions.UnsatisfiedRouteException;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.utils.MessageSourceUtils;

import javax.persistence.OptimisticLockException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Controller
public class GlobalExceptionHandler {

    @Error(global = true, exception = UnsatisfiedRouteException.class)
    public HttpResponse<JsonError> handleUnsatisfiedRoute(UnsatisfiedRouteException e) {
        log.warn(e.getMessage());
        final JsonError error = new JsonError(e.getMessage());

        return HttpResponse.<JsonError>badRequest()
                .body(error);
    }

    @Error(global = true, exception = OptimisticLockException.class)
    public HttpResponse<JsonError> handleOptimisticLock(OptimisticLockException e) {
        log.warn(e.getMessage(), e);
        final JsonError error = new JsonError(e.getMessage());

        return HttpResponse.<JsonError>serverError()
                .body(error);
    }

    @Error(global = true, exception = ConversionErrorException.class)
    public HttpResponse<JsonError> handleConversionError(ConversionErrorException e) {
        log.warn(e.getMessage());
        final JsonError error = new JsonError(e.getMessage());

        return HttpResponse.<JsonError>badRequest()
                .body(error);
    }

    @Error(global = true, exception = ConstraintViolationException.class)
    public HttpResponse<JsonError> handleConstraintViolation(HttpRequest request, ConstraintViolationException e) {
        log.warn(e.getMessage());

        final Map<String, List<String>> warns = new HashMap<>();
        e.getConstraintViolations().forEach(v -> warns.put(propKey(v), Collections.singletonList(e.getMessage())));

        final List<String> violationsMessages = MessageSourceUtils.violationsMessages(e.getConstraintViolations());

        final JsonError error = new JsonError(violationsMessages.stream().collect(Collectors.joining()));

        return HttpResponse.<JsonError>badRequest()
                .body(error);
    }

    // Constraint exception key (Please correct as necessary)
    private String propKey(ConstraintViolation<?> v) {
        String key = v.getPropertyPath().toString();
        if (0 > key.indexOf('.')) {
            return key;
        }
        return key.substring(key.lastIndexOf('.') + 1);
    }

    @Error(global = true, exception = ValidationException.class)
    public HttpResponse<JsonError> handleValidation(ValidationException e) {
        log.warn(e.getMessage());
        final JsonError error = new JsonError(e.getMessage());

        return HttpResponse.<JsonError>badRequest()
                .body(error);
    }

    @Error(global = true, exception = AuthenticationException.class)
    public HttpResponse<JsonError> handleThrowable(HttpRequest request, AuthenticationException e) {
        log.error(e.getMessage());

        final JsonError error = new JsonError(e.getMessage());

        return HttpResponse.<JsonError>unauthorized()
                .body(error);
    }

    @Error(status = HttpStatus.NOT_FOUND, global = true)
    public HttpResponse<JsonError> notFound(HttpRequest request) {
        final JsonError error = new JsonError("Page Not Found")
                .link(Link.SELF, Link.of(request.getUri()));

        return HttpResponse.<JsonError>notFound()
                .body(error);
    }

    @Error(global = true, exception = Throwable.class)
    public HttpResponse<JsonError> handleThrowable(Throwable e) {
        log.error("An unexpected exception occurred.", e);
        final JsonError error = new JsonError("A problem might occur in a server side.");

        return HttpResponse.<JsonError>serverError()
                .body(error);
    }
}