package my.idea.pool.utils;

import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public final class HashUtils {

    public static Optional<String> createHash(@NotNull String value) {
        if (value == null)
            return Optional.empty();

        final MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("MD5");
            digest.update(value.getBytes(StandardCharsets.UTF_8));
            byte[] digestBytes = digest.digest();
            return Optional.of(javax.xml.bind.DatatypeConverter.printHexBinary(digestBytes).toLowerCase());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
