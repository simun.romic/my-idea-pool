package my.idea.pool.utils;

import javax.validation.ConstraintViolation;
import javax.validation.Path;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Utils class to extract messages
 */
public class MessageSourceUtils {

    public static List<String> violationsMessages(@NotNull Set<ConstraintViolation<?>> violations) {
        return violations.stream()
                .map(MessageSourceUtils::violationMessage)
                .collect(Collectors.toList());
    }

    private static String violationMessage(@NotNull ConstraintViolation violation) {
        StringBuilder sb = new StringBuilder();
        Path.Node lastNode = lastNode(violation.getPropertyPath());
        if (lastNode != null) {
            sb.append(lastNode.getName());
            sb.append(" ");
        }
        sb.append(violation.getMessage());
        return sb.toString();
    }

    private static Path.Node lastNode(@NotNull Path path) {
        Path.Node lastNode = null;
        for (final Path.Node node : path) {
            lastNode = node;
        }
        return lastNode;
    }
}
