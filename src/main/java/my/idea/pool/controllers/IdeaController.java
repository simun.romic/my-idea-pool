package my.idea.pool.controllers;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.protocols.IdeaCreateRequest;
import my.idea.pool.protocols.IdeaDto;
import my.idea.pool.protocols.IdeaUpdateRequest;
import my.idea.pool.services.IdeaService;
import my.idea.pool.services.UserService;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Controller("/ideas")
@AllArgsConstructor
@Secured(SecurityRule.IS_AUTHENTICATED)
@Slf4j
@Validated
public class IdeaController {
    private final IdeaService ideaService;

    private final UserService userService;

    /**
     *
     * @param ideaCreateRequest An instance of {@link IdeaCreateRequest} present in the request
     * @return An AccessRefreshToken encapsulated in the HttpResponse or a failure indicated by the HTTP status
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Post
    public HttpResponse<IdeaDto> create(@Valid @Body IdeaCreateRequest ideaCreateRequest, Authentication authentication) {
        return this.userService.findById(authentication.getName()).map(user -> {
            final IdeaDto idea = this.ideaService.create(ideaCreateRequest, user);
            return HttpResponse.created(idea);
        }).orElseGet(HttpResponse::serverError);
    }

    /**
     *
     * @param id
     * @param authentication
     * @return
     */
    @Delete("/{id}")
    public HttpResponse delete(@my.idea.pool.validations.constraints.UUID String id, Authentication authentication) {
        return this.ideaService.findById(UUID.fromString(id)).map(idea -> {
            ideaService.deleteById(idea.getId());
            return HttpResponse.noContent();
        }).orElseGet(HttpResponse::notFound);
    }

    /**
     *
     * @param id
     * @param ideaUpdateRequest
     * @param authentication
     * @return
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Put("/{id}")
    public HttpResponse update(@my.idea.pool.validations.constraints.UUID String id, @Valid @Body IdeaUpdateRequest ideaUpdateRequest, Authentication authentication) {
        final UUID ideaId = UUID.fromString(id);
        return this.ideaService.findById(ideaId).map(idea -> {
            final IdeaDto updatedIdea = ideaService.update(ideaId, idea, ideaUpdateRequest);
            return HttpResponse.ok().body(updatedIdea);
        }).orElseGet(HttpResponse::notFound);
    }

    /**
     *
     * @param page
     * @param authentication
     * @return
     */
    @Get
    public HttpResponse search(@QueryValue(value = "page") Optional<Integer> page, Authentication authentication) {
        final Integer pageNumber = page.orElse(1);
        if (pageNumber <= 0) return HttpResponse.badRequest(new JsonError("page number must be > 0"));
        return HttpResponse.ok(this.ideaService.search(pageNumber));
    }
}
