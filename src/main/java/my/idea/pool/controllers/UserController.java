package my.idea.pool.controllers;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.authentication.providers.AuthoritiesFetcher;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.validation.Validated;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.protocols.UserDto;
import my.idea.pool.protocols.UserRegistrationRequest;
import my.idea.pool.security.jwt.generator.JwtAccessRefreshTokenGenerator;
import my.idea.pool.services.JwtRefreshTokenService;
import my.idea.pool.services.UserService;

import javax.validation.Valid;

@Controller
@Secured(SecurityRule.IS_ANONYMOUS)
@Validated
@AllArgsConstructor
@Slf4j
public class UserController {

    private final UserService userService;

    private final JwtAccessRefreshTokenGenerator jwtAccessRefreshTokenGenerator;

    private final AuthoritiesFetcher authoritiesFetcher;

    private final JwtRefreshTokenService jwtRefreshTokenService;

    /**
     * Endpoint for creating new user and jwt access/refresh tokens
     * Also store refresh token to db
     * @param userRegistrationRequest An instance of {@link UserRegistrationRequest} in the body payload
     * @param request                     The {@link HttpRequest} being executed
     * @return An JwtAccessRefreshToken encapsulated in the HttpResponse or a failure indicated by the HTTP status
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Post("/users")
    public Single<HttpResponse> register(@Valid @Body UserRegistrationRequest userRegistrationRequest, HttpRequest<?> request) {
        if (this.userService.exists(userRegistrationRequest.getEmail())) return Single.just(HttpResponse.badRequest(new JsonError("Email already taken")));

        return Single.fromCallable(() -> this.userService.create(userRegistrationRequest))
                .subscribeOn(Schedulers.io())
                .flatMap(user -> Single.fromPublisher(this.authoritiesFetcher.findAuthoritiesByUsername(user.getUsername()))
                            .map(authorities -> this.jwtAccessRefreshTokenGenerator.generateJwt(new UserDetails(user.getUsername(), authorities)))
                            .map(optionalToken -> {
                                this.jwtRefreshTokenService.create(user.getEmail(), optionalToken);
                                return optionalToken;
                            })
                )
                .map(optionalToken -> optionalToken.map(HttpResponse::created).orElseGet(HttpResponse::serverError));
    }

    /**
     * Endpoint for getting current authenticated user
     * @param authentication An instance of {@link Authentication} in the body payload
     * @param request                     The {@link HttpRequest} being executed
     * @return An User encapsulated in the HttpResponse or a failure indicated by the HTTP status
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Get("/me")
    @Secured(SecurityRule.IS_AUTHENTICATED)
    public HttpResponse<UserDto> me(Authentication authentication, HttpRequest<?> request) {
        return this.userService.byId(authentication.getName())
                .map(HttpResponse::ok)
                .orElseGet(HttpResponse::serverError);
    }
}
