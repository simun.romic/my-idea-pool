package my.idea.pool.controllers;

import io.micronaut.context.annotation.Requires;
import io.micronaut.context.event.ApplicationEventPublisher;
import io.micronaut.core.util.StringUtils;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.*;
import io.micronaut.security.config.SecurityConfigurationProperties;
import io.micronaut.security.event.LoginFailedEvent;
import io.micronaut.security.event.LoginSuccessfulEvent;
import io.micronaut.security.handlers.LoginHandler;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.security.token.jwt.validator.JwtTokenValidator;
import io.micronaut.validation.Validated;
import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.functions.Function;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.protocols.TokenRefreshRequest;
import my.idea.pool.protocols.UserLogoutRequest;
import my.idea.pool.security.authentication.EmailPasswordCredentials;
import my.idea.pool.security.jwt.generator.JwtAccessRefreshTokenGenerator;
import my.idea.pool.security.jwt.render.JwtAccessRefreshToken;
import my.idea.pool.services.JwtRefreshTokenService;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

/**
 *
 * A controller that handles token refresh.
 *
 */
@Requires(property = SecurityConfigurationProperties.PREFIX + ".enabled", value = StringUtils.TRUE)
@Controller("/access-tokens")
@Secured(SecurityRule.IS_ANONYMOUS)
@Validated
@Slf4j
@AllArgsConstructor
public class OAuthController {

    private final Authenticator authenticator;
    private final LoginHandler loginHandler;
    private final ApplicationEventPublisher eventPublisher;

    private final JwtTokenValidator tokenValidator;
    private final JwtAccessRefreshTokenGenerator jwtAccessRefreshTokenGenerator;
    private final JwtRefreshTokenService jwtRefreshTokenService;

    /**
     *
     * @param tokenRefreshRequest An instance of {@link TokenRefreshRequest} present in the request
     * @return An AccessRefreshToken encapsulated in the HttpResponse or a failure indicated by the HTTP status
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Post("/refresh")
    public Single<HttpResponse<JwtAccessRefreshToken>> refresh(@Valid @Body TokenRefreshRequest tokenRefreshRequest) {
        final Flowable<Authentication> authenticationFlowable = Flowable.fromPublisher(tokenValidator.validateToken(tokenRefreshRequest.getRefreshToken()));
        return authenticationFlowable.map((Function<Authentication, HttpResponse<JwtAccessRefreshToken>>) authentication -> {
            final Map<String, Object> claims = authentication.getAttributes();
            final Optional<JwtAccessRefreshToken> jwtAccessRefreshToken = jwtAccessRefreshTokenGenerator.generateJwt(claims, false);
            return jwtAccessRefreshToken.map(HttpResponse::ok).orElseGet(HttpResponse::serverError);
        }).first(HttpResponse.status(HttpStatus.BAD_REQUEST));
    }

    /**
     * @param usernamePasswordCredentials An instance of {@link UsernamePasswordCredentials} in the body payload
     * @param request                     The {@link HttpRequest} being executed
     * @return An AccessRefreshToken encapsulated in the HttpResponse or a failure indicated by the HTTP status
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Post
    public Single<HttpResponse> login(@Valid @Body EmailPasswordCredentials usernamePasswordCredentials, HttpRequest<?> request) {
        final Flowable<AuthenticationResponse> authenticationResponseFlowable = Flowable.fromPublisher(authenticator.authenticate(usernamePasswordCredentials));

        return authenticationResponseFlowable.map(authenticationResponse -> {
            if (authenticationResponse.isAuthenticated()) {
                final UserDetails userDetails = (UserDetails) authenticationResponse;
                eventPublisher.publishEvent(new LoginSuccessfulEvent(userDetails));
                return loginHandler.loginSuccess(userDetails, request);
            }

            final AuthenticationFailed authenticationFailed = (AuthenticationFailed) authenticationResponse;
            eventPublisher.publishEvent(new LoginFailedEvent(authenticationFailed));
            return loginHandler.loginFailed(authenticationFailed);
        }).first(HttpResponse.status(HttpStatus.UNAUTHORIZED));
    }

    /**
     * Logout current user and delete refresh token
     * @param authentication              Currently logged in user
     * @param request                     The {@link HttpRequest} being executed
     * @return HttpResponse or a failure indicated by the HTTP status
     */
    @Consumes(MediaType.APPLICATION_JSON)
    @Delete
    @Secured(SecurityRule.IS_AUTHENTICATED)
    public HttpResponse logout(@Body @Valid UserLogoutRequest userLogoutRequest, Authentication authentication, HttpRequest<?> request) {
        if (!this.jwtRefreshTokenService.refreshTokenExists(userLogoutRequest.getRefreshToken())) return HttpResponse.badRequest(new JsonError("Wrong refresh token!"));
        this.jwtRefreshTokenService.deleteByRefreshToken(userLogoutRequest.getRefreshToken());
        return HttpResponse.noContent();
    }
}