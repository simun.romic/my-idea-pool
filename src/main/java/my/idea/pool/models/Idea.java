package my.idea.pool.models;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "ideas")
public class Idea extends AbstractDomainEntity {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "uuid", updatable = false)
    private UUID id;

    @Size(max = 255)
    private String content;

    @Min(0)
    @Max(10)
    @NotNull
    @Column(nullable = false)
    private Integer impact;

    @Min(0)
    @Max(10)
    @NotNull
    @Column(nullable = false)
    private Integer ease;

    @Min(0)
    @Max(10)
    @NotNull
    @Column(nullable = false)
    private Integer confidence;

    @NotNull
    @Column(name = "average_score", nullable = false)
    private Double averageScore;

    @ManyToOne
    @JoinColumn(name="user_id", referencedColumnName="email", nullable = false)
    @NotNull
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = false)
    private Date createdAt;
}
