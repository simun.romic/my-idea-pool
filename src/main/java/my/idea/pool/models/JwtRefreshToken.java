package my.idea.pool.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents JWT tokens entity
 */
@Entity
@Table(name = "jwt_refresh_tokens", indexes = { @Index(name = "IDX_REFRESH_TOKEN", columnList = "refresh_token")} )
@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class JwtRefreshToken extends AbstractDomainEntity {

    @Id
    @SequenceGenerator(name = "jwtRefreshTokenSeqGen", sequenceName = "jwtRefreshTokenSeq")
    @GeneratedValue(generator = "jwtRefreshTokenSeqGen")
    @NotNull
    private long id;

    @NotBlank
    @Column(name = "refresh_token", nullable = false, unique = true)
    private String refreshToken;

    @ManyToOne
    @JoinColumn(name="user_id", referencedColumnName="email", nullable = false)
    @NotNull
    private User user;
}
