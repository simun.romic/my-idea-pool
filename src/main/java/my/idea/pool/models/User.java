package my.idea.pool.models;

import io.micronaut.security.authentication.providers.UserState;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(callSuper=true)
@Entity
@Table(name = "users")
public class User extends AbstractDomainEntity implements UserState {

    @Id
    @NotBlank
    @Email
    @Column(nullable = false, unique = true, updatable = false)
    private String email;

    @NotBlank
    @Column(nullable = false)
    private String name;

    @NotBlank
    @Column(nullable = false)
    private String password;

    @NotBlank
    @Column(name = "avatar_url", nullable = false)
    private String avatarUrl;

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean isAccountExpired() {
        return false;
    }

    @Override
    public boolean isAccountLocked() {
        return false;
    }

    @Override
    public boolean isPasswordExpired() {
        return false;
    }
}
