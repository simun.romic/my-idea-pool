package my.idea.pool.repositories;

import io.micronaut.cache.annotation.Cacheable;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.runtime.ApplicationConfiguration;
import my.idea.pool.models.User;
import my.idea.pool.protocols.UserDto;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import java.util.Optional;

@Singleton
public class UserRepository extends AbstractGenericRepository<User, String> {

    public UserRepository(@CurrentSession EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        super(entityManager, applicationConfiguration, User.class);
    }

    @Cacheable
    public Optional<UserDto> getDtoById(@NotNull String id) {
        final TypedQuery<UserDto> q = super.entityManager.createQuery(
                "SELECT new my.idea.pool.protocols.UserDto(u.email, u.name, u.avatarUrl) FROM User u WHERE u.email = :id",
                UserDto.class);
        q.setParameter("id", id);
        return Optional.ofNullable(q.getSingleResult());
    }

    @Cacheable
    public boolean exists(@NotNull String id) {
        final TypedQuery<Boolean> q = super.entityManager.createQuery(
                "SELECT case when count(u) > 0 then true else false end FROM User u WHERE u.email = :id",
                Boolean.class);
        q.setParameter("id", id);
        return q.getSingleResult();
    }
}
