package my.idea.pool.repositories;

import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.runtime.ApplicationConfiguration;
import my.idea.pool.models.JwtRefreshToken;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

@Singleton
public class JwtRefreshTokenRepository extends AbstractGenericRepository<JwtRefreshToken, Long> {
    protected JwtRefreshTokenRepository(@CurrentSession EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        super(entityManager, applicationConfiguration, JwtRefreshToken.class);
    }

    public void deleteByRefreshToken(@NotNull String refreshToken) {
        final Query q = super.entityManager.createQuery(
                "DELETE FROM JwtRefreshToken t WHERE t.refreshToken = :refreshToken");
        q.setParameter("refreshToken", refreshToken);
        q.executeUpdate();
    }

    public Boolean refreshTokenExists(@NotNull String refreshToken) {
        final TypedQuery<Boolean> q = super.entityManager.createQuery(
                "SELECT case when count(t) > 0 then true else false end FROM JwtRefreshToken t WHERE t.refreshToken = :refreshToken",
                Boolean.class);
        q.setParameter("refreshToken", refreshToken);
        return q.getSingleResult();
    }
}
