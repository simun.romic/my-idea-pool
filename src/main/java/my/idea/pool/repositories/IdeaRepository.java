package my.idea.pool.repositories;

import io.micronaut.cache.annotation.CacheConfig;
import io.micronaut.cache.annotation.Cacheable;
import io.micronaut.configuration.hibernate.jpa.scope.CurrentSession;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import io.micronaut.runtime.ApplicationConfiguration;
import my.idea.pool.models.Idea;
import my.idea.pool.protocols.IdeaDto;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.UUID;

@Requires(property="ideas.max-page-size")
@Singleton
@CacheConfig("ideas-cache")
public class IdeaRepository extends AbstractGenericRepository<Idea, UUID> {

    @Value("${ideas.max-page-size}")
    private int maxPageSize;

    public IdeaRepository(@CurrentSession EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        super(entityManager, applicationConfiguration, Idea.class);
    }

    @Cacheable
    public List<IdeaDto> search(int page) {
        if (page < 0) throw new IllegalArgumentException("page must be > 0");

        final TypedQuery<IdeaDto> q = super.entityManager.createQuery(
                "SELECT new my.idea.pool.protocols.IdeaDto(i.id, i.content, i.impact, i.ease, i.confidence, i.averageScore, i.createdAt) FROM Idea i ORDER BY i.id",
                IdeaDto.class)
                .setMaxResults(maxPageSize)
                .setFirstResult((page-1) * maxPageSize);
       return  q.getResultList();
    }
}
