package my.idea.pool.repositories;

import io.micronaut.cache.annotation.CacheInvalidate;
import io.micronaut.cache.annotation.CachePut;
import io.micronaut.cache.annotation.Cacheable;
import io.micronaut.runtime.ApplicationConfiguration;
import lombok.extern.slf4j.Slf4j;
import my.idea.pool.models.AbstractDomainEntity;

import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import java.util.Optional;

/**
 * Abstract repository
 * @param <T> Entity type
 * @param <K> Entity's key type
 */
@Slf4j
public abstract class AbstractGenericRepository<T extends AbstractDomainEntity, K> {
    protected final EntityManager entityManager;

    protected final ApplicationConfiguration applicationConfiguration;

    protected final Class<T> clazz;

    protected AbstractGenericRepository(EntityManager entityManager, ApplicationConfiguration applicationConfiguration, Class<T> clazz) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
        this.clazz = clazz;
    }

    @Cacheable
    public Optional<T> findById(@NotNull K id) {
        log.info("Id to fetch: {}", id);

        return Optional.ofNullable(this.entityManager.find(this.clazz, id));
    }

    @CachePut
    public T create(@NotNull T t) {
        log.info("create: {}", t);
        this.entityManager.persist(t);
        return t;
    }

    @CacheInvalidate
    public T update(@NotNull T t) {
        return this.entityManager.merge(t);
    }

    @CacheInvalidate
    public void deleteById(@NotNull K id) {
        findById(id).ifPresent(this.entityManager::remove);
    }
}
