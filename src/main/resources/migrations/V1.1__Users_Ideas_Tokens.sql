--
-- PostgreSQL database dump
--

-- Dumped from database version 11.3 (Debian 11.3-1.pgdg90+1)
-- Dumped by pg_dump version 11.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ideas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ideas (
    id uuid NOT NULL,
    version bigint NOT NULL,
    average_score double precision NOT NULL,
    confidence integer NOT NULL,
    content character varying(255),
    created_at timestamp without time zone NOT NULL,
    ease integer NOT NULL,
    impact integer NOT NULL,
    user_id character varying(255) NOT NULL,
    CONSTRAINT ideas_confidence_check CHECK (((confidence >= 0) AND (confidence <= 10))),
    CONSTRAINT ideas_ease_check CHECK (((ease >= 0) AND (ease <= 10))),
    CONSTRAINT ideas_impact_check CHECK (((impact >= 0) AND (impact <= 10)))
);


ALTER TABLE public.ideas OWNER TO postgres;

--
-- Name: jwt_refresh_tokens; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jwt_refresh_tokens (
    id bigint NOT NULL,
    version bigint NOT NULL,
    refresh_token character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.jwt_refresh_tokens OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    email character varying(255) NOT NULL,
    version bigint NOT NULL,
    avatar_url character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: ideas ideas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ideas
    ADD CONSTRAINT ideas_pkey PRIMARY KEY (id);


--
-- Name: jwt_refresh_tokens jwt_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jwt_refresh_tokens
    ADD CONSTRAINT jwt_refresh_tokens_pkey PRIMARY KEY (id);


--
-- Name: jwt_refresh_tokens uk_1qs2nrl7ljlvfe00xrvhnt13t; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jwt_refresh_tokens
    ADD CONSTRAINT uk_1qs2nrl7ljlvfe00xrvhnt13t UNIQUE (refresh_token);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (email);


--
-- Name: idx_refresh_token; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_refresh_token ON public.jwt_refresh_tokens USING btree (refresh_token);


--
-- Name: jwt_refresh_tokens fkgq9hl7ucpl62c631h47ydbfx3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jwt_refresh_tokens
    ADD CONSTRAINT fkgq9hl7ucpl62c631h47ydbfx3 FOREIGN KEY (user_id) REFERENCES public.users(email);


--
-- Name: ideas fkt4qp1368gdn4wk6ih62bj80ym; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ideas
    ADD CONSTRAINT fkt4qp1368gdn4wk6ih62bj80ym FOREIGN KEY (user_id) REFERENCES public.users(email);


--
-- PostgreSQL database dump complete
--

