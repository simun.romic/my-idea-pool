BEGIN;

-- CREATE SEQUENCE "jwtrefreshtokenseq" ------------------------
CREATE SEQUENCE "public"."jwtrefreshtokenseq"
INCREMENT 50
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
-- -------------------------------------------------------------

COMMIT;


BEGIN;

-- CREATE SEQUENCE "uuid" --------------------------------------
CREATE SEQUENCE "public"."uuid"
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
-- -------------------------------------------------------------

COMMIT;

BEGIN;

-- CREATE SEQUENCE "hibernate_sequence" ------------------------
CREATE SEQUENCE "public"."hibernate_sequence"
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1;
-- -------------------------------------------------------------

COMMIT;
